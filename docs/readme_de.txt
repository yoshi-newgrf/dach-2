DACH Set

Version 0.9 - Februar 2015

Detaillierte Informationen werden noch ver�ffentlicht.

Credits
=======
DACH:
Grafiken: Matthias G�rtler
Code: Matthias G�rtler, angelehnt an den Code vom SBB Set von Daniel Plaumann (Dandan). 

Deutsche Texte von Matthias G�rtler

Besonderer Dank an David Hilker (officercrockey) f�r die Hilfe bei den bisherigen Versionen und
an Henry Willett (SwissFan91) f�r die Unterst�tzung bei der Recherche.

License
=======
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this NewGRF; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.