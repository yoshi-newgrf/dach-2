//============================================================
// ID208 DB-Schnellzugwagen
//============================================================

//
//PROPERTIES
//

item(FEAT_TRAINS, item_coach_DB_Schnellzugwagen) {
    property {
        name:						string(STR_COACH_DB_SCHNELLZUGWAGEN);
		introduction_date:			date(1959,01,10);
        model_life:					VEHICLE_NEVER_EXPIRES; //should extend 5 years after the intended end year
		retire_early:				1;
        vehicle_life:				50;
        climates_available: 		ALL_CLIMATES;

        length: 					8;
        dual_headed: 				0;

        speed: 						200 km/h;
        weight: 					42 ton;

		refittable_cargo_classes: 	bitmask(CC_PASSENGERS);
		default_cargo_type: 		PASS;	
        cargo_capacity: 			66;
        refit_cost: 				0;
        loading_speed:				60; //set via global setting
		
        sprite_id: 					SPRITE_ID_NEW_TRAIN;
        bitmask_vehicle_info: 		0;
	}
}



//------------------------------------------------------------


switch(FEAT_TRAINS, SELF, sw_replace_coach_DB_Schnellzugwagen, [STORE_TEMP(num_vehs_in_consist - position_in_consist - position_in_consist - 1, 0x10F), var[0x61, 0, 0x0000FFFF, 0xC6]]){
    item_coach_DB_n_Wagen: 				sw_coach_DB_n_Wagen_sprites_backward;
    item_coach_DB_DoSto: 				sw_coach_DB_dosto_sprites_backward;
    item_coach_DB_Fernverkehrswagen:	sw_coach_DB_Fernverkehrswagen_sprites_backward;
	item_coach_OBB_DoSto:				sw_coach_OBB_dosto_sprites_backward;
	item_coach_OBB_langeSchlieren:		sw_coach_OBB_langeschlieren_sprites_backward;
	item_luggage_DB_Fahrradwagen:		sw_luggage_DB_Fahrradwagen_sprites_backward;
	item_coach_DB_Schnellzugwagen:		sw_coach_DB_Schnellzugwagen_sprites_backward;
										sw_coach_DB_Schnellzugwagen_sprites_backward;
}

switch(FEAT_TRAINS, SELF, sw_coach_DB_Schnellzugwagen_backward_checkdisplay, extra_callback_info1 & 0x11){
    0x11: sw_coach_DB_Schnellzugwagen_sprites_forward;
          sw_replace_coach_DB_Schnellzugwagen;
}

switch(FEAT_TRAINS, SELF, sw_coach_DB_Schnellzugwagen_backward_checkcabcar, [count_veh_id (item_coach_DB_n_Wagen_STW) 
																			+ count_veh_id (item_coach_DB_DoSto_STW)
																			+ count_veh_id (item_coach_DB_Fernverkehrswagen_STW)
																			+ count_veh_id (item_coach_OBB_DoSto_STW)
																			+ count_veh_id (item_coach_OBB_langeSchlieren_STW)]){
    0: 	sw_coach_DB_Schnellzugwagen_sprites_forward;
        sw_coach_DB_Schnellzugwagen_backward_checkdisplay;
}

switch(FEAT_TRAINS,PARENT, sw_coach_DB_Schnellzugwagen_sprites,vehicle_is_reversed){
    0: sw_coach_DB_Schnellzugwagen_sprites_forward;
	   sw_coach_DB_Schnellzugwagen_backward_checkcabcar;
}

//Purchase menu sprite
switch(FEAT_TRAINS,SELF,sw_coach_DB_Schnellzugwagen_purchase,current_year){
    0..1986:    spriteset_coach_DB_Schnellzugwagen_purchase_eilzug;
    1987..2005: spriteset_coach_DB_Schnellzugwagen_purchase_interregio;
                spriteset_coach_DB_Schnellzugwagen_purchase_rot;
}

//CARGO SUBTYPE TEXT

switch(FEAT_TRAINS,SELF,sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e_i_r_a,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_AUTOMATIC);
    1: return string(STR_CARGO_SUBTYPE_EILZUG_2);
    2: return string(STR_CARGO_SUBTYPE_EILZUG_1);
    3: return string(STR_CARGO_SUBTYPE_INTERREGIO);
    4: return string(STR_CARGO_SUBTYPE_INTERREGIO_RESTAURANT);
    5: return string(STR_CARGO_SUBTYPE_ROT);
    6: return string(STR_CARGO_SUBTYPE_ALEX);
    7: return string(STR_CARGO_SUBTYPE_ALEX_RESTAURANT);
       return CB_RESULT_NO_TEXT;
}
switch(FEAT_TRAINS,SELF,sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e_i_r,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_AUTOMATIC);
    1: return string(STR_CARGO_SUBTYPE_EILZUG_2);
    2: return string(STR_CARGO_SUBTYPE_EILZUG_1);
    3: return string(STR_CARGO_SUBTYPE_INTERREGIO);
    4: return string(STR_CARGO_SUBTYPE_INTERREGIO_RESTAURANT);
    5: return string(STR_CARGO_SUBTYPE_ROT);
       return CB_RESULT_NO_TEXT;
}
switch(FEAT_TRAINS,SELF,sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e_i,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_AUTOMATIC);
    1: return string(STR_CARGO_SUBTYPE_EILZUG_2);
    2: return string(STR_CARGO_SUBTYPE_EILZUG_1);
    3: return string(STR_CARGO_SUBTYPE_INTERREGIO);
    4: return string(STR_CARGO_SUBTYPE_INTERREGIO_RESTAURANT);
       return CB_RESULT_NO_TEXT;
}
switch(FEAT_TRAINS,SELF,sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_AUTOMATIC);
    1: return string(STR_CARGO_SUBTYPE_EILZUG_2);
    2: return string(STR_CARGO_SUBTYPE_EILZUG_1);
       return CB_RESULT_NO_TEXT;
}


switch(FEAT_TRAINS,SELF,sw_coach_DB_Schnellzugwagen_cargo_subtype_text,current_year){
    0..1986:    sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e;
    1987..2005: sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e_i;
    2006: 		sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e_i_r;
                sw_coach_DB_Schnellzugwagen_cargo_subtype_text_e_i_r_a;
}


//------------------------------------------------------------


//
//ITEM GRAPHICS
//

item(FEAT_TRAINS, item_coach_DB_Schnellzugwagen) {
    graphics {
    additional_text: 			return(string(STR_PURCHASE_PAXWAGON_WITH_LIVERIES_DESC,string(STR_PURCHASE_TYPE_PAXWAGON),string(STR_COACH_DB_SCHNELLZUGWAGEN_USAGE),string(STR_COACH_DB_SCHNELLZUGWAGEN_EOS),string(STR_COACH_DB_SCHNELLZUGWAGEN_LIVERIES),string(STR_COACH_DB_SCHNELLZUGWAGEN_DESC)));
	cargo_subtype_text:			sw_coach_DB_Schnellzugwagen_cargo_subtype_text;
	cargo_age_period:			sw_cargo_age_period_normal;
	loading_speed:				sw_loading_speed_normal;
    purchase: 					sw_coach_DB_Schnellzugwagen_purchase;
	purchase_running_cost_factor:sw_coach_DB_Schnellzugwagen_purchase_running_cost;
	running_cost_factor:		sw_coach_DB_Schnellzugwagen_running_cost;
    default:					sw_coach_DB_Schnellzugwagen_sprites;
    }
}